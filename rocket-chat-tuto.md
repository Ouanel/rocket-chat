% Prise en main de Rocket Chat
% Alternatiba
% Avril 2018

# Sommaire

----

* S'inscrire
* Rejoindre un salon public
* Discuter
* Créer un salon

---

# Pour commencer

----

## S'inscrire

----

* Aller sur [chat.alternatiba.eu](https://chat.alternatiba.eu/)
* Cliquez sur "Créer un nouveau compte"

![](media/01-rocket-chat-login.png) 

----

Entrez vos informations et validez

![](media/02-rocket-chat-inscription.png)

----

Choisissez un pseudo

![](media/03-rocket-chat-choix-pseudo.png)

----

Et bienvenue :)

![](media/04-rocket-chat-accueil.png)

----

## Discuter

----

Rejoignez le salon général en cliquant sur son nom

![](media/04-rocket-chat-canal-general.png)

----

1. Écrire votre message
2. Appuyer sur entrée

![](media/06-rocket-chat-canal-message.png)

----

## Rejoindre un canal de discussion public

----

### Canal public

* Cliquer sur ![](media/04-rocket-chat-menu-liste-discussions.png) pour afficher la liste
* Cliquez sur le canal que vous souhaitez rejoindre. 

![](media/08-rocket-chat-canal-liste.png)

----

Vous obtenez un aperçu du canal : cliquez sur "rejoindre" pour confirmer.

![](media/08-rocket-chat-canal-rejoindre.png)

----

![](media/08-rocket-chat-canal-rejoindre-confirme.png)

----

### Groupe privé

* Sur invitation
* Groupes privés pas présents sur la liste

<aside class="notes">

Un membre du groupe doit vous inviter à venir discuter dans le groupe privé. Les groupes privés ne sont pas listés dans la liste des canaux.

</aside>

---

# Utilisation régulière

----

## Mentionner des personnes

-   Entrez `@` puis les premières lettres du pseudo
-   Pour notifier tout le salon, écrivez `@all` puis votre message

![](media/07-rocket-chat-canal-mention-3.png)

* **Attention** un mail est envoyé à votre destinataire s'il n'est pas connecté
* **Attention** `@all` ne fonctionne pas pour des salons **> 100 personnes**

----

![](media/07-rocket-chat-canal-mention-4.png)

<aside class="notes">

Pour notifier une personne directement, utilisez le @pseudo de la
personne. Elle recevra une alerte.

Pour mentionner toutes les personnes du salon, utilisez le @all. 
Le serveur est configuré pour ne pas envoyer de notifications 
si le salon contient plus de 100 personnes.

</aside>

----

## Retrouver ses “mentions” dans une discussion

* Dans le menu du canal de discussion en haut à droite, cliquez sur "mentions" dans le menu

![](media/07-rocket-chat-canal-mention-liste.png)

<aside class="notes">

Vous n'avez pas consulté le chat depuis un moment et pourtant des gens vont ont interpellé dans les discussions en utilisant votre pseudo (@pseudo) ?

Retrouvez l'ensemble de ces interpellations ou “mentions” avec le bouton “@” dans le menu de droite, appelé aussi “mentions”.

En cliquant sur le symbole “petite roue crantée” à côté du pseudo de celui qui vous mentionne, vous avez une petite main qui s'affiche et vous permet d'arriver directement à l'endroit de la discussion où vous avez été mentionné.

</aside>

----

## Afficher la liste des membres


* Dans le menu en haut à droite cliquer sur ![](media/09-rocket-chat-canal-membres-menu.png)

![](media/09-rocket-chat-canal-membres-liste.png)


## Entamer une conversation privée avec un autre membre

* Cliquer sur le nom du destinataire
* Cliquer sur "conversation"

![](media/12-rocket-chat-discussion-personne.png)

---

# Gestion des préférences

----

## Accéder aux préférences

Pour éditer les préférences, cliquer sur votre avatar puis "Mon compte"

![](media/10-rocket-chat-preferences.png)

----

## Éditer le nom affiché

* Aller dans "Mon compte"
* Cliquer sur Profil

![](media/10-rocket-chat-preferences-profil.png)

----

## Recevoir des alertes mails

Par défaut vous recevez les notifications par mail à chaque message perso ou à chaque “mention” de votre pseudo lorsque vous êtes déconnecté.

Pour désactiver ces alertes, aller dans les préférences et sélectionner "Désactivé"

![](media/10-rocket-chat-preferences-mail.png)

<aside class="notes">

Une fois que vous avez vérifié votre mail à la création du compte (ou ensuite à travers la configuration de votre profil), vous recevrez par défaut des alertes mails à chaque message perso ou à chaque “mention” de votre pseudo lorsque vous êtes déconnecté.

Vous pouvez ensuite aussi configurer les alertes par salon via les
options de notifications à travers l'icone cloche sur la droite (il est
même possible de recevoir par mail tous les messages d'un salon)

</aside>

---

# Gestion des canaux et modération

----

## Créer un Canal

-   cliquer sur ![](media/04-rocket-chat-menu-creer-canal.png)
-   remplissez les informations

![](media/11-rocket-chat-canal-creation.png)

<aside class="notes">

Pour créer un nouveau canal, cliquer sur le bouton d'édition dans le menu de gauche. Le créateur du canal en devient le seul propriétaire.

Il est possible de créer un canal public ou privé, et de modifier ses paramètres par la suite, donc attention à la confidentialité de vos échanges et la vie privée de vos interlocuteurs !

</aside>

----

## Modifier un canal

Cliquer sur ![](media/09-rocket-chat-canal-infos-menu.png) puis sur "Modifier" pour éditer les paramètres du canal.

Le créateur d'un canal peut désigner un⋅e ou plusieurs propriétaires (_owner_) supplémentaires

<aside class="notes">

Il est possible de modifier le nom d'un canal avec le bouton “i” qui est tout en haut à droite. Seuls les propriétaires d'un canal peuvent le faire (mais il faut qu'ils aient vérifié leur mail).

Il est possible d'éditer l'en-tête du salon pour y mettre les informations principales

</aside>
----

## Modération sur un salon

Celui qui lance un salon est “propriétaire du salon”. 
Dès lors il peut :

-   Ajouter des modérateurs
-   Modifier le nom d'un canal (seulement si il a vérifié son mail)

----

Les modérateurs (et aussi le propriétaire) peuvent :

-   Exclure des utilisateurs
-   Les rendre muets
-   Modifier le nom d'un canal (seulement si il a vérifié son mail)

----

## Merci !

----

Issu de [Wikibooks - Construire des communs / chat](https://fr.wikibooks.org/wiki/Construire_des_communs/Contribution/Chat)

*Simon Sarazin, Jack Potte, Numahell, Suzy,…*

<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/fr/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/fr/88x31.png" /></a><br />Ce contenu est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/fr/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 3.0 France</a>.
