# Présentation outil Rocket.Chat

## Générer le diaporama

```
git clone --recursive
pandoc -t html -s index.md -o public/index.html
pandoc -t revealjs -s presentation-outil.md -o public/presentation-outil.html --self-contained
pandoc -t revealjs -s rocket-chat-tuto.md -o public/rocket-chat-tuto.html --self-contained
```

## Contribuer

Les messages de commit sont en français pour ce projet.
