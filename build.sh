
pandoc -t html -s index.md -o public/index.html
pandoc -t revealjs -s presentation-outil.md -o public/presentation-outil.html --self-contained -V theme=beige
pandoc -t revealjs -s rocket-chat-tuto.md -o public/rocket-chat-tuto.html --self-contained -V theme=beige
